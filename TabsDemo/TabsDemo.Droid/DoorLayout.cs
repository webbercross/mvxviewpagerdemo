﻿using System;
using Android.Content;
using Android.Util;
using Android.Widget;

namespace TabsDemo.Droid
{
    public class DoorLayout : FrameLayout
    {
        public DoorLayout(Context context)
            : base(context)
        {

        }

        public DoorLayout(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {

        }

        public DoorLayout(Context context, IAttributeSet attributeSet, int defStyleAttr)
            : base(context, attributeSet, defStyleAttr)
        {

        }

        public DoorLayout(Context context, IAttributeSet attributeSet, int defStyleAttr, int defStyleRes)
            : base(context, attributeSet, defStyleAttr, defStyleRes)
        {

        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            base.OnMeasure(widthMeasureSpec, widthMeasureSpec);
        }
    }
}
