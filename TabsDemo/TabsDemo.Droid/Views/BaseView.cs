using Android.OS;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;

namespace TabsDemo.Droid.Views
{
    public abstract class BaseView : MvxActivity
    {
        protected Toolbar Toolbar { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(LayoutResource);

            //Toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            //if (Toolbar != null)
            //{
            //    SetActionBar(Toolbar);
            //    ActionBar.SetDisplayHomeAsUpEnabled(true);
            //    ActionBar.SetHomeButtonEnabled(true);
            //}
        }

        protected abstract int LayoutResource { get; }
    }
}
