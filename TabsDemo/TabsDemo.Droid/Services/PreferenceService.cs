﻿using Android.Content;
using MvvmCross.Platforms.Android;
using TabsDemo.Core.Services;

namespace TabsDemo.Droid.Services
{
    public class PreferenceService : IPreferenceService
    {
        IMvxAndroidCurrentTopActivity _activity;

        public PreferenceService(
            IMvxAndroidCurrentTopActivity activity)
        {
            _activity = activity;
        }

        public string Name
        {
            get => GetSharedPreferences().GetString(nameof(Name), null);
            set => GetSharedPreferencesEditor().PutString(nameof(Name), value).Commit();
        }

        public SortEnum SortEnum
        {
            get => (SortEnum)GetSharedPreferences().GetInt(nameof(SortEnum), (int)SortEnum.Up);
            set => GetSharedPreferencesEditor().PutInt(nameof(SortEnum), (int)value).Commit();
        }

        ISharedPreferences GetSharedPreferences()
        {
            return _activity.Activity.GetSharedPreferences(nameof(PreferenceService), FileCreationMode.Private);
        }

        ISharedPreferencesEditor GetSharedPreferencesEditor()
        {
            return _activity.Activity.GetSharedPreferences(nameof(PreferenceService), FileCreationMode.Private).Edit();
        }
    }
}
