using MvvmCross.IoC;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.ViewModels;
using TabsDemo.Core;
using TabsDemo.Core.Services;
using TabsDemo.Droid.Services;

namespace TabsDemo.Droid
{
    public class Setup : MvxAndroidSetup<App>
    { 
        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }

        protected override IMvxIoCProvider InitializeIoC()
        {
            var provider = base.InitializeIoC();

            provider.RegisterType<IPreferenceService, PreferenceService>();

            return provider;
        }
    }
}
