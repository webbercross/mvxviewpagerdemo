﻿using MvvmCross.DroidX.RecyclerView.ItemTemplates;
using TabsDemo.Core.ViewModels;

namespace TabsDemo.Droid
{
    public class DoorTemplateSelector : IMvxTemplateSelector
    {
        public int ItemTemplateId { get; set; } 

        public int GetItemViewType(object item)
        {
            var vm = (DoorViewModel)item;

            if (vm.Number % 2 == 0)
                return 1;

            return -1;
        }

        public int GetItemLayoutId(int viewType)
        {
            if (viewType == 1)
                return Resource.Layout.DoorLayout2;

            return ItemTemplateId;
        }
    }
}
