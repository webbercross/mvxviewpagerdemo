﻿using Android.OS;
using Android.Views;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views.Fragments;
using TabsDemo.Core.ViewModels;

namespace TabsDemo.Droid.Fragments
{
    public class Tab3Fragment : MvxFragment<Tab3ViewModel>
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            return this.BindingInflate(Resource.Layout.Tab3, container, false);
        }
    }
}
