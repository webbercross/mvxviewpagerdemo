﻿using Android.OS;
using Android.Views;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views.Fragments;
using TabsDemo.Core.ViewModels;

namespace TabsDemo.Droid.Fragments
{
    public class Tab1Fragment : MvxFragment<Tab1ViewModel>
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            return this.BindingInflate(Resource.Layout.Tab1, container, false);
        }
    }
}
