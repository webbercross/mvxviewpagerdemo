﻿using System.Collections.Generic;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.ViewPager.Widget;
using Google.Android.Material.Tabs;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Platforms.Android.Views.Fragments;
using MvvmCross.Platforms.Android.Views.ViewPager;
using MvvmCross.ViewModels;
using TabsDemo.Core.ViewModels;

namespace TabsDemo.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(FirstViewModel), Resource.Id.tabs_frame)]
    public class TabsFragment : MvxFragment<TabsViewModel>
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.TabLayout, container, false);

            var viewPager = view.FindViewById<ViewPager>(Resource.Id.viewpager);
            if (viewPager != null)
            {
                var fragments = new List<MvxViewPagerFragmentInfo>()
                {
                    new MvxViewPagerFragmentInfo("Tab A", null, typeof(Tab1Fragment), new MvxViewModelRequest(typeof(Tab1ViewModel))),
                    new MvxViewPagerFragmentInfo("Tab B", null, typeof(Tab2Fragment), new MvxViewModelRequest(typeof(Tab2ViewModel))),
                    new MvxViewPagerFragmentInfo("Tab C", null, typeof(Tab3Fragment), new MvxViewModelRequest(typeof(Tab3ViewModel)))
                };
                viewPager.Adapter = new MvxCachingFragmentStatePagerAdapter(Activity, ChildFragmentManager, fragments);
            }

            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            tabLayout.SetupWithViewPager(viewPager);

            for(var i = 0; i < tabLayout.TabCount; i++)
            {
                var tab = tabLayout.GetTabAt(i);
                tab.SetCustomView(Resource.Layout.CustomTab);

                var header = tab.CustomView.FindViewById<LinearLayout>(Resource.Id.tab_header);
                var title = tab.CustomView.FindViewById<TextView>(Resource.Id.tab_title);

                if(i == 0)
                {
                    header.SetBackgroundResource(Resource.Drawable.tab_a);
                    title.SetText("Tab A", TextView.BufferType.Normal);
                    title.SetTextColor(Color.Black);
                }
                else if (i == 1)
                {
                    header.SetBackgroundResource(Resource.Drawable.tab_b);
                    title.SetText("Tab B", TextView.BufferType.Normal);
                    title.SetTextColor(Color.Black);
                }
                else if (i == 2)
                {
                    header.SetBackgroundResource(Resource.Drawable.tab_c);
                    title.SetText("Tab C", TextView.BufferType.Normal);
                    title.SetTextColor(Color.Black);
                }
            }

            return view;
        }
    }
}
