﻿using MvvmCross.ViewModels;

namespace TabsDemo.Core.ViewModels
{
    public class DoorViewModel : MvxViewModel
    {
        private bool _isExpanded;

        public int Number { get; set; }
        public bool IsExpanded
        {
            get => _isExpanded;
            set { SetProperty(ref _isExpanded, value); }
        }
    }
}
