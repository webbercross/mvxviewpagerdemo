using System.Collections.Generic;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace TabsDemo.Core.ViewModels
{
    public class Tab1ViewModel : MvxViewModel
    {
        public IMvxCommand<DoorViewModel> SelectDoorCommand { get; }

        public List<DoorViewModel> Doors { get; } = new List<DoorViewModel>
        {
            new DoorViewModel { Number = 1 },
            new DoorViewModel { Number = 2 },
            new DoorViewModel { Number = 3 },
            new DoorViewModel { Number = 4 },
            new DoorViewModel { Number = 5 },
            new DoorViewModel { Number = 6 },
            new DoorViewModel { Number = 7 },
            new DoorViewModel { Number = 8 },
            new DoorViewModel { Number = 9 },
            new DoorViewModel { Number = 10 }
        };

        public Tab1ViewModel()
        {
            SelectDoorCommand = new MvxCommand<DoorViewModel>(OnDoorSelected);
        }

        void OnDoorSelected(DoorViewModel door)
        {
            
        }
    }

    public class Tab2ViewModel : MvxViewModel
    {
        public IMvxCommand<DoorViewModel> SelectDoorCommand { get; }

        public List<DoorViewModel> Doors { get; } = new List<DoorViewModel>
        {
            new DoorViewModel { Number = 1 },
            new DoorViewModel { Number = 2 },
            new DoorViewModel { Number = 3 },
            new DoorViewModel { Number = 4 },
            new DoorViewModel { Number = 5 },
            new DoorViewModel { Number = 6 },
            new DoorViewModel { Number = 7 },
            new DoorViewModel { Number = 8 },
            new DoorViewModel { Number = 9 },
            new DoorViewModel { Number = 10 }
        };

        public Tab2ViewModel()
        {
            SelectDoorCommand = new MvxCommand<DoorViewModel>(OnDoorSelected);
        }

        void OnDoorSelected(DoorViewModel door)
        {
            door.IsExpanded = !door.IsExpanded;
        }
    }

    public class Tab3ViewModel : MvxViewModel
    {

    }

    public class TabsViewModel
        : MvxViewModel
    {
    }
}
