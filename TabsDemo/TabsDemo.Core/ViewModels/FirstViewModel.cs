using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using TabsDemo.Core.Services;

namespace TabsDemo.Core.ViewModels
{
    public class FirstViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IPreferenceService _preferenceService;

        public List<DoorViewModel> Doors { get; } = new List<DoorViewModel>
        {
            new DoorViewModel { Number = 1 },
            new DoorViewModel { Number = 2 },
            new DoorViewModel { Number = 3 },
            new DoorViewModel { Number = 4 },
            new DoorViewModel { Number = 5 },
            new DoorViewModel { Number = 6 },
            new DoorViewModel { Number = 7 },
            new DoorViewModel { Number = 8 },
            new DoorViewModel { Number = 9 },
            new DoorViewModel { Number = 10 }
        };

        public FirstViewModel(
            IMvxNavigationService navigationService,
            IPreferenceService preferenceService)
        {
            _navigationService = navigationService;
            _preferenceService = preferenceService;
        }

        public override async Task Initialize()
        {
            var name = _preferenceService.Name;
            var sort = _preferenceService.SortEnum;

            _preferenceService.Name = "Geoff";
            _preferenceService.SortEnum = SortEnum.Down;

            await _navigationService.Navigate<TabsViewModel>();

            await base.Initialize();
        }
    }
}
