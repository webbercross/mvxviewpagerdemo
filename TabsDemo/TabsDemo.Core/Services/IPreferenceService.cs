﻿namespace TabsDemo.Core.Services
{
    public enum SortEnum
    {
        Up,
        Down
    }

    public interface IPreferenceService
    {
        string Name { get; set; }
        SortEnum SortEnum { get; set; }
    }
}
